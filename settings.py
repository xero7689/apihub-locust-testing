import os
from dotenv import load_dotenv

load_dotenv()

STAGE = os.environ.get('DEPLOY_STAGE', 'at-dev')
END_POINT = f"https://{STAGE}-api.orcahub-dev.com"
USER_IDENTITY_FILE = ''

ORCA_MBS = {
    "at-dev": {
        "API_KEY": "",
        "PROJECT_ID": "",
        "IDENTITY_POOL_ID": "",
        "IDENTITY_ID": "",
        "CLIENT_ID": "",
    },
    "at-ut": {
        "API_KEY": "",
        "PROJECT_ID": "",
        "IDENTITY_POOL_ID": "",
        "IDENTITY_ID": "",
        "CLIENT_ID": "",
    },
    "at-uat": {
        "API_KEY": "",
        "PROJECT_ID": "",
        "IDENTITY_POOL_ID": "",
        "IDENTITY_ID": "",
        "CLIENT_ID": ""
    }
}

ORCA_MBS = ORCA_MBS[STAGE]
