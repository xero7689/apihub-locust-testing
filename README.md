# apihub-locust-testing

## Prepare User Identity List File
Example:
```
9ee44e0d-xxxx-4955-a845-872f762e0fbd
9244d30d-xxxx-4df7-b3ad-16b5c9b9a424
22c03866-xxxx-4c64-8e4d-17da600c3297
2359fb9c-xxxx-41d3-a93c-12f1f6f9121d
e48377dd-xxxx-4b9e-8775-5c8505b895ea
f0ad8503-xxxx-4846-9b36-cd6eb3146687
...
```

## Fulfill Settings File
`PROJECT_ID`
`IDENTITY_POOL_ID`
`IDENTITY_ID`
`API_KEY`
`CLIENT_ID`
`USER_IDENTITY_FILE`

## Environment Variable
`DEPLOY_STAGE`: Target stage to do load test
`LOCUST_MASTER_HOST`: Define this variable on the slave node

## Execute Master Node
```
docker-compose up master
```

## Execute Slave Node
```
docker-compose up worker
```
