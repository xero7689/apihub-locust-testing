import json
import time
import random

from locust import HttpUser, task, tag

from settings import *

users = []
with open(USER_IDENTITY_FILE, 'r') as f:
    for line in f.readlines():
        users.append(line.strip('\n'))

PICKED_USER_NUM = 400
users = random.sample(users, PICKED_USER_NUM)
random.seed(int(time.time()))

class GroupPointUser(HttpUser):
    @tag('new_wallet')
    @task
    def new_wallet(self):
        API_PATH = "/grouppoint/NewWallet"
        identity_id = random.choice(users)
        payload = {
            "Context": {
                "ProjectId": ORCA_MBS['PROJECT_ID'],
                "IdentityPoolId": ORCA_MBS['IDENTITY_POOL_ID'],
                "IdentityId": identity_id
            },
        }

        headers = {
            "Content-Type": "application/json",
            "Authorization": f'ApiKey {ORCA_MBS["API_KEY"]}'
        }
        self.client.post(API_PATH, headers=headers, data=json.dumps(payload))

    @tag('get_point')
    @task
    def get_point(self):
        API_PATH = "/grouppoint/GetPoint"
        identity_id = random.choice(users)
        payload = {
            "Context": {
                "ProjectId": ORCA_MBS['PROJECT_ID'],
                "IdentityPoolId": ORCA_MBS['IDENTITY_POOL_ID'],
                "IdentityId": identity_id
            },
        }

        headers = {
            "Content-Type": "application/json",
            "Authorization": f'ApiKey {ORCA_MBS["API_KEY"]}'
        }
        self.client.post(API_PATH, headers=headers, data=json.dumps(payload))
